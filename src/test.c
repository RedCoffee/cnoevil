#include "Evil.h"

comment("Create a function");
Fn(int, add) uses
  Int(a), Int(b)
Begin
  Int(retval) = a + b;
  return(retval);
End

comment("Use Main's shorthand");
Main

  comment("Check argc and argv");

  comment("If");
  If add(2, 2) != 4
  Begin
    println("If failed.");
    return(-1);
  End
  
  comment("If/Else");
  If add(2, 2) == 16
  Begin
    println("If/Else failed");
    return(-1);
  Else
    pass;
  End
  
  comment("while");
  
  comment("for");
  
  comment("foreach");
  
  comment("caseof");

  return(0);
End