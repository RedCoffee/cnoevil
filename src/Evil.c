#include "Evil.h"

Fn(int, add) uses
  Int(a), Int(b)
Begin
  Int(retval) = a + b;
  return(retval);
End

Main
  println("I'm in main!");
  println(add(2, 4));
  return(0);
End
