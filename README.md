CNoEvil
=======

Goal: Using the C-Preprocessor, create a new, easy to use language.

Yes, this is a bad idea. However, it's also kind of fun to abuse macros to see what madness we can create.

---

## Hello, World!

The obligatory program that every language show to new users, so they can get a feel for the syntax.

```
#include "CNoEvil.h"

Main
  println("Hello, World!");
End
```

This is directly equivalent to C's:

```
int64_t main(int64_t argc, char** argv)
{
  printf("%s\n", "Hello, World!");
}
```

Things of note:

* Main automatically exposes argc and argv.
* We remove the ```{}``` brackets, instead using a syntax somewhat similar to Lua or Ruby.
* int is replaced with a fixed-width integer relevant to your system, of 64, 32 or 16bits.
* Clang uses ```int```, it doesn't expand the type.

---

# Language

## Tradeoffs and inner workings

* CNoEvil attempts to use ```int64_t```, ```int32_t``` or ```int16_t``` instead of ```int```, for less surprises.
* ```Begin``` and ```Then``` are interchangeable, so you can choose the best according to readability.
* ```Elif``` exists, making C's ```} else if() {``` easier to deal with. But ```Else``` is equivalent to ```} else {```, meaning it can make nested Ifs break in unexpected ways.
* ```print``` and ```println``` only take a single argument.
* You will still need to understand C's memory management model. We don't aim to be a dynamic language. If a function appears dynamic, like ```println```, its using ```_Generic```.
* Not every C pattern or syntax is implemented, but if you can't find it, you can mix C and CNoEvil without any issues.

## Goals

First up, if you use this in production, you are utterly insane, and should probably re-evaluate your decision making processes.

Now that's out of the way, the goals of CNoEvil are:

* Have fun! Abusing the C Pre-Processor is awesome fun.
* Remove braces. (Use Lua/Ruby style blocks).
* Use *only* the C Pre-Processor. Nothing but macros.
* Use _Generic wherever possible to give the language a semblance of type inference.

## Functions

The macros that CNoEvil gives you access to are:

### ```comment```

Takes any number of valid C identifiers and produces comments, (so they appear when you're debugging). 

Example:
```
comment("This is a comment!");
comment("This is a multiline",
"comment!");
```

### ```print```

Takes a single variable of any* type, and prints to stdout.

Example:

```
print(Int(2));
print("Hi");
print(Double(0.8));
```

### ```println```

The same as ```print```, except it appends a newline to the end of stdout.


### ```Fn```

Used to create a function definition.

Takes:

* A C type and a function name
* A block, followed by an ```End```

Example:

```
Fn(int, add) uses
  Int(a), Int(b)
Begin
  Int(retval) = a + b;
  return(retval);
End

println(add(2, 4));
```

Notes:

* ```uses``` is a keyword for clarity's sake. It can be omitted.
* A block, beginning with ```Begin```, continuing to ```End``` is mandatory.
* A ```return``` call is mandatory.

### ```Type```

Used to annotate a variable to any available C-Type.

Exists purely to make syntax more uniform, as standard C notation still works.

Example:

```
Type(SDL_Renderer, *renderer);
```

Is directly equivalent to:

```
SDL_Renderer *renderer;
```

### ```Struct```

Simply a keyword, as a struct's initialisation is different than most types.

Example:

```
Struct this {
  Int(a);
};
```

### ```Union```

Simply a keyword, as an union's initialisation is different than most types.

Example:

```
Union this {
  Int(a);
};
```

### ```Int```

Used to annotate a type, as ```int```.

Example:

```
Int(a);
```

is equal to C's ```int a;```

### ```Float```

Used to annotate a type, as ```float```.

Example:

```
Float(a);
```

is equal to C's ```float a;```

### ```Double```

Used to annotate a type, as ```double```.

Example:

```
Double(a);
```

is equal to C's ```double a;```

### ```Char```

Used to annotate a type, as ```char```.

Example:

```
Char(a);
```

is equal to C's ```char a;```

### ```Void```

Used to annotate a type, as ```void```.

Example:

```
Void(a);
```

is equal to C's ```void a;```

### ```If```

Used to create a conditional block.

Takes:

* A truthy statement
* A ```Begin``` block to evaluate
* An ```End``` statement or an ```Else``` block or an ```Elif``` block

Example:

```
If 1 != 0
Then
  println("Reality holds!");
Elif 1 == 42
Then
  println("What magical world is this?");
Else
  println("Oh hell... This compiler is insane...");
End
```

### ```pass```

A basic no-op, similar to Python's pass.

Example:

```
If 1 == 0
Begin
  println("What the hell?!");
Else
  pass;
End
```

### ```While```

Used to create a while loop.

Takes:

* A truthy statement
* A ```Begin``` block to evaluate

Example:

```
Int(i) = 0;
While i < 25
Begin
  i++;
  print(i);
  println("Whoo!");
End
```

### ```For```

Used to create a for loop.

Takes:

* An initial iterator, an iterator comparison, an iterator change value.
* A ```Begin``` block

Example:

```
For Int(i) = 0 and i < 25 and i++;
Begin
  print(i);
  println("Whoo!");
End
```

### ```Switch```

Switch/case statements are a powerful tool of C. You should familiarise yourself with it before using this.

Example:

```
Int(a) = 4;
Switch(a)
  Case(0)
    println("Huh?");
    break;
  End
  Case(4)
    println("Whoo!");
    break;
  End
  Default
    println("Aww...");
    break;
  End
End
```

## Usage

```
#include "Evil.h"
```

That's it. All that's required to turn C into a monstrosity, is to include this one little header file.

An example might be:

```
#include "Evil.h"

Fn(int, add) uses
  Int(a), Int(b)
Begin
  Int(retval) = a + b;
  return(retval);
End

Main
  println(add(12, 32));
  return(0);
End
```

Compile it with the C11 standard, for example:

```
gcc -std=c11 src/Evil.c -o Evil
```

---

# License

See the file, [LICENSE.md](LICENSE.md).
