#include "Evil.h"
#include "SDL.h"
#include <time.h>

SDL_Window *window;
SDL_Renderer *renderer;

Int(WINDOW_WIDTH) = 510;
Int(WINDOW_HEIGHT) = 510;

Int(BLOCK_WIDTH) = 30;

Int(BLOCKS_X) = 17;
Int(BLOCKS_Y) = 17;

Int(LEVEL)[17][17];

Int(SHAPES)[5][8] = {
	{
		0, 0,
		1, 0,
		2, 0,
		3, 0
	},
	{
		0, 0,
		1, 0,
		1, 1,
		2, 1
	},
	{
		0, 0,
		1, 0,
		2, 0,
		2, 1
	},
	{
		0, 0,
		1, 0,
		0, 1,
		1, 1
	},
	{
		0, 0,
		1, 0,
		2, 0,
		1, 1
	}
};

Fn(int, max) uses
  Int(a), Int(b)
Begin
  If a >= b
  Then
    return(a);
  Else
    return(b);
  End
End

Fn(int, min) uses
  Int(a), Int(b)
Begin
  If a <= b
  Then
    return(a);
  Else
    return(b);
  End
End

Fn(SDL_Window*, create_window)
Begin
  Int(posX) = SDL_WINDOWPOS_CENTERED;
  Int(posY) = SDL_WINDOWPOS_CENTERED;

  return SDL_CreateWindow(
	"SDL Window",
	posX,
	posY,
	WINDOW_WIDTH,
	WINDOW_HEIGHT,
	SDL_WINDOW_OPENGL);
End

Fn(SDL_Renderer*, create_renderer) uses
  SDL_Window *window
Begin
  return SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
End

Fn(void, destroy_and_quit) uses
  SDL_Window *window, SDL_Renderer *renderer
Begin
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
End

Fn(int, input_quit) uses
  SDL_Event *event
Begin
  SDL_PollEvent(event);
  If event->type == SDL_QUIT
  Then
    println("Quitting.");
    return 1;
  Else
    return 0;
  End
End

Fn(void, draw_level_blocks) uses
  SDL_Texture **level_texture
Begin
  SDL_SURFACE *surface;
  SDL_RECT r;

  r.w = 1;
  r.h = 1;
  r.x = 0;
  r.y = y;

  surface = SDL_CreateRGBSurface(0, 17, 17, 32, 0, 0, 0, 250);
  For Int(y) = 0 and y < 17 and y++ Then
    For Int(x) = 0 and x <17 and x++ Then
      If LEVEL[y][x] == 1
      Then
        r.x = x;
        r.y = y;
        SDL_FillRect(surface, &r, SDL_MapRGB(surface->format, 250, 0, 0))'
      End
    End
  End

  *level_texture = SDL_CreateTextureFromSurface(renderer, surface);
End

Fn(void, clear_line) uses
  Int(y)
Begin
  For Int(i) = y and i >= 0 and i-- Then
    For Int(x) = 0 and x < 17 and x++ Then
      If i == 0
      Then
        LEVEL[i][x] = 0;
      Else
        LEVEL[i][x] = LEVEL[i-1][x];
      End
    End
  End
End

Fn(int, get_completed_line_pos)
Begin
  For Int(y) = 0 and y < 17 and y++ Then
    For Int(x) =0 and x < 17 and x++ Then
      If x == 16 && LEVEL[y][x] == 1
      Then
        return(y);
      Else
        If LEVEL[y][x] != 1
        Then
          break;
        End
      End
    End
  End
  return(-1);
End

Fn(void, clear_lines)
Begin
  Int(y) = get_completed_line_pos();
  While y >= 0
  Begin
    clear_line(y);
    y = get_completed_line_pos();
  End
End

// CNoEvil has no structs yet
struct Shape {
	SDL_Texture **texture;
	SDL_Rect *rect;
	Int(blocks_x);
	Int(blocks_y);

	Double(pos_x);
	Double(pos_y);

	Int(blocks_position)[8];
};

Fn(void, create_shape) uses
  struct Shape *shape, Int(pos_x), Int(pos_y)
Begin
  SDL_Surface *surface;
  SDL_Rect r, *rect;

  rect = malloc(sizeof(struct SDL_Rect));

  r.w = 1;
  r.h = 1;
  r.x = 0;
  r.y = 0;

  Int(y);
  Int(x);
  surface = SDL_CreateRGBSurface(0, shape->blocks_y, shape->blocks_y, 32, 0, 0, 0, 0);
  For Int(i) = 0 and i < 8 and i += 1 Then
    r.x = shape->blocks_position[i];
    r.y = shape->blocks_position[i+1];
    SDL_FillRect(surface, &r, SDL_MapRGB(surface->format, 250, 0, 0));
  End

  shape->pos_x = pos_x;
  shape->pos_y = pos_y;
  rect->x = shape->pos_x;
  rect->y = shape->pos_y;
  rect->w = BLOCK_WIDTH * shape->blocks_x;
  rect->h = BLOCK_WIDTH * shape->blocks_y;

  shape->texture = SDL_CreateTextureFromSurface(renderer, surface);
  shape->rect = rect;
  SDL_FreeSurface(surface);

End

Fn(void, set_blocks_x_y) uses
  struct Shape *shape, Int(blocks_position)[]
Begin
  shape->blocks_x = 0;
  shape->blocks_y = 0;

  For Int(x) =  0 and x < 8 and x+=2 Then
    shape->blocks_x = max(blocks_position[x] + 1, shape->blocks_x);
    shape->blocks_y = max(blocks_position[x+1] + 1, shape->blocks_y);
  End
End

Fn(void, free_shape) uses
  struct Shape *shape
Begin
  SDL_DestroyTexture(shape->texture);
  free(shape->rect);
End

Fn(void, get_new_shape) uses
  struct Shape *shape
Begin
  Int(r) = rand() % 5;

  set_blocks_x_y(shape, SHAPES[r]);

  memcpy(shape->blocks_position, Shapes[r], sizeof(int) * 8);

  Int(pos_x) = rand() % (WINDOW_WIDTH / BLOCK_WIDTH - shape->blocks_x + 1) * BLOCK_WIDTH;
  Int(pos_y) = 0;
  create_shape(shape, pos_x, pos_y);
End

Fn(void, make_non_negative) uses
  Int(*new_blocks);
Begin
  Int(i);
  Int(min_x) = 0;
  Int(min_y) = 0;

  For i = 0 and i < 8 and i += 2 Then
    min_x = min(new_blocks[i], min_x);
    min_y = min(new_blocks[i+1], min_y);
  End

  For i = 0 and i < 8 and i += 2 Then
    If min_x < 0
    Then
      new_blocks[i] += -1 * min_x;
    End

    If min_y < 0
    Then
      new_blocks[i+1] += -1 * min_y;
    End
  End
End

Fn(int, *rotate) uses
  Int(blocks_position)[]
Begin
  Int(*new_blocks);
  new_blocks = malloc(8 * sizeof(int));
  new_blocks[0] = blocks_position[0];
  new_blocks[1] = blocks_position[1];
  Int(x);
  Int(y);
  For Int(i) = 0 and i < 8 and i+=2 Then
    x = blocks_position[0];
    y = blocks_position[1];
    Int(new_x) = x + (y - blocks_position[i+1]);
    Int(new_y) = y + -1 * (x - blocks_position[i]);
    new_blocks[i] = new_x;
    new_blocks[i+1] = new_y;
    printf("x: %i, y: %i\n", blocks_position[i], blocks_position[i+1]);
    printf("x: %i, y: %i\n", new_blocks[i], new_blocks[i+1]);
  End
  make_non_negative(new_blocks);
  return(new_blocks);
End

Fn(void, rotate_shape) uses
  struct Shape *shape
Begin
  Int(*new_blocks);
  new_blocks = rotate(shape->blocks_position);

  free_shape(shape);

  set_blocks_x_y(shape, new_blocks);

  memcpy(shape->blocks_position, new_blocks, sizeof(int) * 8);

  create_shape(shape, shape->pos_x, shape->pos_y);

  free(new_blocks);
End

Fn(double, get_time_elapsed) uses
  clock_t t_start, clock_t t_now
Begin
  Double(time_diff) = (double)(t_now - t_start) / CLOCKS_PER_SEC;
  return(time_diff);
End

Fn(void, freeze_shape) uses
  struct Shape *shape
Begin
  Int(x);
  Int(y);

  For Int(i) = 0 and i < 8 and i += 2 Then
    x = shape->rect->x / BLOCK_WIDTH + shape->blocks_position[i];
    y = shape->rect-> y / BLOCK_WIDTH + shape->blocks_position[i+1];
    LEVEL[y][x] = 1;
  End
End

Fn(int, check_y_collision) uses
  struct Shape *shape
Begin
  Int(x);
  Int(y);

  For Int(i) = 0 and i < 8 and i += 2 Then
    x = shape->rect->x / BLOCK_WIDTH + shape->blocks_position[i];
    y = shape->rect->y / BLOCK_WIDTH + shape->blocks_position[i+1];
    If LEVEL[y+1][x] == 1 || y == 16
    Then
      return(1);
    End
  End

  return(0);
End

Fn(int, check_x_collision) uses
  struct Shape *shape, Int(x_offset)
Begin
  Int(x);
  Int(y);

  For Int(i) = 0 and i < 8 and i += 2 Then
    x = shape->rect->x / BLOCK_WIDTH + shape->blocks_position[i];
    y = shape->rect->y / BLOCK_WIDTH + shape->blocks_position[i+1];

    If LEVEL[y][x + x_offset] == 1
    Then
      return(1);
    End
  End

  return(0);
End

Fn(int, is_game_lost)
Begin
  For Int(i) = 0 and i < 17 and i++ Then
    If LEVEL[0][i] == 1
    Then
      return(1);
    End
  End
  return(0);
End

Fn(void, clear_level)
Begin
  For Int(y) = 0 and y < 17 and y++ Then
    For Int(x) = 0 and x < 17 and x++ Then
      LEVEL[y][x] = 0;
    End
  End
End

Fn(void, move_down) uses
  Double(seconds_elapsed), struct Shape *shape
Begin
  static clock_t last_move = 0;
  If get_time_elapsed(last_move, clock()) >= 0.2)
  Then
    shape->pos_y = shape->pos_y + BLOCK_WIDTH;
    shape->rect->y = shape->pos_y;
    last_move = clock();
  End
End

Fn(void, move_right) uses
  struct Shape *shape
Begin
  static clock_t last_keypress = 0;
  If get_time_elapsed(last_keypress, clock()) > 0.1
  Then
    If shape->pos_x < WINDOW_WIDTH - (BLOCK_WIDTH * shape->blocks_x))
    Then
      If check_x_collision(shape, 1))
      Then
        return;
      End
      shape->pos_x = shape->pos_x + BLOCK_WIDTH;
      shape->rect->x = shape->pos_x;
      last_keypress = clock();
    End
  End
End

Fn(void, move_left) uses
  struct Shape *shape
Begin
  static clock_t last_keypress = 0;
  If get_time_elapsed(last_keypress, clock()) > 0.1
  Then
    If shape->pos_x > 0 && !check_x_collision(shape, -1)
    Then
      shape->pos_x = shape->pos_x - BLOCK_WIDTH;
      shape->rect->x = shape->pos_x;
      last_keypress = clock();
    End
  End
End

Fn(void, draw) uses
  struct Shape *shape, SDL_Texture *level_texture
Begin
  SDL_Rect r;
  r.w = BLOCK_WIDTH * 17;
  r.h = BLOCK_WIDTH * 17;
  r.x = 0;
  r.y = 0;

  SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
  SDL_RenderClear(renderer);

  SDL_RenderCopy(renderer, level_texture, NULL, &r);
  SDL_RenderCopy(renderer, shape->texture, NULL, shape->rect);
  SDL_RenderPresent(renderer);
End

Fn(void, body)
Begin
  SDL_Event event;
  struct Shape *shape;
  SDL_Texture *level_texture;

  draw_level_blocks(&level_texture);

  shape = malloc(sizeof(struct Shape));
  get_new_shape(shape);

  clock_t time, time_now, last_keypress;
  double seconds_elapsed;
  time = clock();

  While 1
  Then
    time_now = clock();
    seconds_elapsed = get_time_elapsed(time, time_now);
    time = time_now;

    SDL_PollEvent(&event);
    If event.type == SDL_QUIT
    Then
      println("Quitting.");
      break;
    End

    If check_y_collision(shape)
    Then
      freeze_shape(shape);
      clear_lines();

      If is_game_lost()
      Then
        clear_level();
      End

      draw_level_blocks(&level_texture);
      free_shape(shape);
      get_new_shape(shape);
    End

    If event.type == SDL_KEYDOWN
    Then
      If event.key.keysym.scancode == SDL_SCANCODE_LEFT
      Then
        move_left(shape);
      Elif event.key.keysym.scancode == SDL_SCANCODE_RIGHT
      Then
        move_right(shape);
      Elif event.key.keysym.scancode == SDL_SCANCODE_SPACE
      Then
        rotate_shape(shape);
      End
    End

    move_down(seconds_elapsed, shape);
    draw(shape, level_texture);
  End
End

Main
  srand(time(NULL) + clock());

  If SDL_Init(SDL_INIT_VIDEO) == 0
  Then
    window = create_window();
    renderer = create_renderer(window);
    body();
    destroy_and_quit(window, renderer);
  Else
    return(-1);
  End

  return(0);
End
