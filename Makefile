CC=gcc
FLAGS=-std=c11
OUTPUT=Evil
FILES=src/Evil.c

clean:
	-rm $(OUTPUT)

all:
	$(CC) $(FLAGS) $(FILES) -o $(OUTPUT)
